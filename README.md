# README #

iOS Coding Skills Challenge from "AppsPeople"

The sample application should be a valid Xcode project, sent as a ZIP file attachment. Make sure you include all resources so that your project can be compiled and run on iPhone Xcode Simulator or on an up-to-date iPhone.  Make sure your application is bug-free and easy-to-use for a basic user.  Comments in the code are more than welcome and the use of storyboard(s) is MANDATORY. If you do not have time to complete all the features, please provide for each missing feature a few sentences about how you planned to implement it. 
The application should have an icon and a static splash screen (you can use images from Google Images) and you should cater for retina as well as third generation devices. The application should have a bottom tab bar to allow navigating between these different screens, which should include icons: 
UI Screen: it should display two fields (username, password) and their titles. A validation should be performed on the length of the password (more than 3 letters) 
Table screen: it should display a scrollable list of 10 website names. On the right of each website an arrow. When one website is touched, it opens a new page displaying the website. There should be a way to go back to the previous screen from here. Page should be scalable. 
Image screen: it contains a button that, when clicked, asks the user to make one of three choices, choose a photo from their photo library, take a photograph and to cancel. Once the photo is selected/taken, the photo should be displayed in the same page.
